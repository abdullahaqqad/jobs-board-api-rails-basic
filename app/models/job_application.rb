class JobApplication < ApplicationRecord
  belongs_to :job_post
  validates_presence_of :created_by
  validates :status, acceptance: { accept: %w[not_seen seen] }
end
