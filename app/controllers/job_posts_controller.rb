class JobPostsController < ApplicationController
  load_and_authorize_resource
  before_action :set_job_post, only: %i[show update destroy]

  # GET /job_posts
  def index
    @jobPost = JobPost.all
    json_response(@jobPost)
  end

  # GET /job_posts/:id
  def show
    json_response(@jobPost, :ok)
  end

  # POST /job_posts/:id
  def create
    @jobPost = current_user.job_posts.create!(jobPost_params)
    json_response(@jobPost, :created)
  end

  # PATCH /job_posts/:id
  def update
    if @jobPost.user_id == current_user.id
      @jobPost.update(jobPost_params)
    else
      json_response('you dont have access to this post', :fail)
    end
  end

  # DELETE /job_posts/:id
  def destroy
    if @jobPost.user_id == current_user.id
      @jobPost.destroy
      json_response(@jobPost, :destroyed)
      head :no_content
    end
  end

  private

  def jobPost_params
    params.permit(:title, :description)
  end

  def set_job_post
    @jobPost = JobPost.find(params[:id])
  end

end
