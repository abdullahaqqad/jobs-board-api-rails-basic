FactoryBot.define do
  factory :user do
    email { Faker::Name.name }
    password_digest { '9i9i9i9i' }
  end
end
