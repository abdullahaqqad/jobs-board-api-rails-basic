FactoryBot.define do
  factory :job_application do
    created_by { Faker::Number.number(10) }
    status { %w[seen not_seen].sample }
    job_post_id { nil }
  end
end
