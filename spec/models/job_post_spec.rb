require 'rails_helper'

RSpec.describe JobPost, type: :model do

  it { should belong_to(:user) }
  it { should have_many(:job_applications).dependent(:destroy) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }

end
