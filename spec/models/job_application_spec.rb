require 'rails_helper'

RSpec.describe JobApplication, type: :model do

  it { should belong_to(:job_post) }
  it { should validate_presence_of(:created_by) }
  # it { should validates(:status).with_values('seen', 'not seen') }

end
